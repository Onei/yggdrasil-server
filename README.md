# yggdrasil-server

A self-hostable Minecraft authentication server and profile system, aiming to
be compatible with Mojang's official Yggdrasil servers.
Originally intended to be run by individual Minecraft multiplayer servers to provide a
Microsoft-free way to identify players, but could also be run as a more
publicly available instance used by more than one multiplayer server.

The main motivation for this was Microsoft's acquisition of Mojang and, more
recently, the migration forcing Minecraft players to use a Microsoft
account.

## Setup and usage

Assuming you have already cloned this repository and cd'ed into it:

1. Make sure you have Node.js and npm installed
2. Install dependencies with `npm install`
3. Check `config.json` for some options which you may wish to customise
4. Start the server with `npm start` (or `node .` or `node index.js`)
5. To add an account or profile, use the [db-tool.js](./db-tool.js) script
   (try `./db-tool.js help`). (In the not-too-distant future this will instead
   be done via a user-friendly web UI)

To directly connect your Minecraft server or client to it, add this to your
JVM arguments,
replacing `http://localhost:8081` with the yggdrasil-server's root endpoint:

```
-Dminecraft.api.auth.host=http://localhost:8081/auth
-Dminecraft.api.account.host=http://localhost:8081/account
-Dminecraft.api.session.host=http://localhost:8081/session
-Dminecraft.api.services.host=http://localhost:8081/services
```

This will work even with vanilla Minecraft!
However, you will need to do some tweaks to make skins and capes work:
see [TEXTURES.md](./TEXTURES.md) for more info.

Alternatively, you can use a more user-friendly method of logging in per server
via a game mod. This previously existed in the form of
[custom-login](https://modrinth.com/mod/custom-login),
but this is unmaintained and uses too many janky techniques.
Look out for a better solution Coming Soon!

### Passthrough

With `passthrough.enabled` set to `true` in [config.json](./config.json)
(the default), requests which fail locally will be forwarded to the servers
given in `passthrough.servers` instead, which by default correspond to
Mojang Yggdrasil.
This way, players registered locally will be resolved locally, and everyone
else will be resolved remotely.

A possible use case (and the default setup) of this would be on a server where
you want to allow normal Minecraft players to connect, but also make use of
custom authentication.

## Using in production

Currently this code is not at all audited for security, so you should probably
not use this in production anyway.
However, if you really want to, you will definitely want to use some sort of
proxy to enable HTTPS (in the future this may be supported natively).
For example, you could proxy the traffic through `nginx` with SSL set up,
which would also allow you to make the Yggdrasil API available under an
existing website.

Also, as per Express docs, you should set the environment variable
`NODE_ENV` to `production`.

## Contributing

Send issues or `git send-email` patches to me at the email address listed on
[my Codeberg profile](https://codeberg.org/winter) or submit them
[via a pull request](https://codeberg.org/winter/yggdrasil-server/pulls)
