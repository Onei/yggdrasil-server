# Project to-do list

Where applicable, to-dos should be specified in comments next to the code they
affect, prefixed with one of:

- TODO -- for something which needs to be implemented or otherwise modified
- FIXME -- for bugs or other behaviour which needs to be fixed
- HACK -- for code which works but could be made less "hacky"

## TODO: Use a proper logging system

In `index.js` especially, `console.log` is used to output messages to stdout.
It would be better to use multiple logging levels to allow changing how verbose
the output is.

## TODO: Add a styleguide / contributing guidelines

Code style, formatting tools to use, where to send PRs, etc.

## TODO: Improve db-tool.js

Make it more user-friendly, resistant to errors, etc.

## TODO: Implement rate-limiting

Alternatively, suggest the usage of a reverse proxy or something

## TODO: Support emailing users

Given that the account username is already an email address, should be fine.
Definitely a requirement for getting this production-ready -- confirmation
emails on sign up are obviously very useful for anti-spammage
